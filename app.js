const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Ceci est mon application pour mon projet CICD')
})

app.listen(port, () => {
  console.log(`Application est en écoute à http://localhost:${port}`)
})